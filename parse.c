///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file parse.c
/// @version 2.0
///
/// @author Bailey Borengasser <baileykb@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   29/01/2021 
///////////////////////////////////////////////////////////////////////////////
#include "parse.h"

void parse(int argc, char *argv[]) {

   extern bool print_chars;
   extern bool print_words;
   extern bool print_lines;
   
   // Declare variable for getopt_long & switch case
   int opt = 0; 
   
   // Declare struct for long_options(bytes,lines, etc.)
   // Referred to man getopt_long for assistance
   static struct option long_options[] = {
      {"bytes",   required_argument, NULL, 'c'},
      {"lines",   required_argument, NULL, 'l'},
      {"words",   required_argument, NULL, 'w'},
      {"version", no_argument,       NULL, 'v'}, 
      {0,         0,                 0,     0 } 
   };

   while( (opt = getopt_long(argc, argv, ":clwv:", long_options, NULL)) != -1 ){
      
      switch(opt){
         
         // Checks for short option -c or long option --bytes
         case 'c':
            print_chars = true;
            break;
         
         // Checks for short option -l or long option --lines
         case 'l':
            print_lines = true;
            break; 
         
         // Checks for short option -w or long option --words
         case 'w':
            print_words = true; 
            break; 
         
         // Checks for short option -v or long option --version
         case 'v':
            printf("This is version 2 of wc.\n"); 
            exit(EXIT_SUCCESS);   
         
         // Invalid arguments
         case '?':
            fprintf(stderr, "wc: Invalid argument provided.\n"); 
            exit(EXIT_FAILURE); 
         }
      }
   }
