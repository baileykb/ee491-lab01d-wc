///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file check-file.c
/// @version 1.0
///
/// @author Bailey Borengasser <baileykb@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   27/01/2021 
///////////////////////////////////////////////////////////////////////////////
#include "check-file.h"


// This function will check if the passed file exists, if it doesn't we return an error and exit.

void check_file(FILE *file, const char *name){

   if ( file == NULL ){
      fprintf(stderr,"wc: Can't open [%s]\n", name);
      exit(EXIT_FAILURE);
   }
}

