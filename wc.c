///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file wc.c
/// @version 2.0
///
/// @author Bailey Borengasser <baileykb@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   29/01/2021 
//i/////////////////////////////////////////////////////////////////////////////
#include "wc.h" 

// Declare variables to keep track of line, word and character counts
int line_count      = 0;
int word_count      = 0;
int character_count = 0;

// Declare variables to determine what command line arguments are provided
bool print_chars = false;
bool print_words = false; 
bool print_lines = false;

int main(int argc, char *argv[]) {

   // Declare file pointer 
   FILE *file;
   
   // Declare char pointer to hold file name
   const char *file_name; 
 
   // Call parse to parse through the provided command line arguments
   parse(argc, argv); 

   // Loop through command line arguments to open any files 
   for( int i = 1; i < argc; i++ ){
      
      // Check if first character of argv[i][0] != '-', if true, we attempt to open a file at that index otherwise we check next index
      if ( (argv[i][0] != '-')) {
         file_name = argv[i];
         file = fopen(file_name, "r"); 
         check_file(file, file_name); 
         count_file(file); 
         fclose(file); 
         
         // Case if no command line arguments are provided. 
         if ( (print_chars == false) && (print_lines == false) && (print_words == false) ) {
             printf("%d\t %d\t %d\t [%s]\n", line_count, word_count, character_count, file_name); 
         }
            
         // Case if -c or --bytes is provided
         if(print_chars == true){
            printf("%d\t [%s]\n", character_count, file_name);
         }
           
         // Case if --l or --lines is provided
         if(print_lines == true) {
               printf("%d\t [%s]\n", line_count, file_name);
         }
         // Case if -w or --words is provided
         if(print_words == true){
            printf("%d\t [%s]\n", word_count, file_name);
         }
      } 
   
      // Have to reset counter variables to zero before looping to process counts of multiple files correctly
      word_count      = 0; 
      line_count      = 0;
      character_count = 0;
   }

   return (EXIT_SUCCESS);
}
