///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file parse.h
/// @version 2.0
///
/// @author Bailey Borengasser <baileykb@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   29/01/2021 
///////////////////////////////////////////////////////////////////////////////
#ifndef PARSE_H
#define PARSE_H

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <stdbool.h> 

// Function Prototype
void parse(int argc, char *argv[]);

#endif
