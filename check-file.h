///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file check-file.h
/// @version 1.0
///
/// @author Bailey Borengasser <baileykb@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   27/01/2021 
///////////////////////////////////////////////////////////////////////////////
#ifndef CHECKFILE_H
#define CHECKFILE_H

#include <stdio.h>
#include <stdlib.h>

//Function prototype
void check_file(FILE *file, const char* name);  

#endif
