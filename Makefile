# Build a Word Counting program

CC     = gcc
CFLAGS = -g -Wall

TARGET = wc

all: $(TARGET)

wc: wc.c check-file.c count.c parse.c
	$(CC) $(CFLAGS) -o $(TARGET) wc.c check-file.c count.c parse.c

test:
	$(info Note: ./wc program should have the same output as wc program)	
	wc -c test1
	./wc -c test1
	wc --bytes test1
	./wc --bytes test1
	wc -l test2
	./wc -l test2
	wc --lines test2
	./wc --lines test2
	wc -w test3
	./wc -w test3
	wc --words test3
	./wc --words test3
	wc test1
	./wc test1
	./wc --version

clean:
	rm $(TARGET)

