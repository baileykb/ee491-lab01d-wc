///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file wc.h
/// @version 1.0
///
/// @author Bailey Borengasser <baileykb@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   27/01/2021 
///////////////////////////////////////////////////////////////////////////////
#ifndef WC_H
#define WC_H

#include "check-file.h"
#include "count.h"
#include "parse.h"

#endif
