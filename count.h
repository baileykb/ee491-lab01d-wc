///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file count.h
/// @version 1.0
///
/// @author Bailey Borengasser <baileykb@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   29/01/2021 
///////////////////////////////////////////////////////////////////////////////
#ifndef COUNT_H
#define COUNT_H

#include <stdio.h>
#include <stdbool.h> 

// Function Prototype
void count_file(FILE *file);

#endif
