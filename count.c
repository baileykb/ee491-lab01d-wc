///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 01 - WordCount
///
/// @file count.c
/// @version 2.0
///
/// @author Bailey Borengasser <baileykb@hawaii.edu>
/// @brief  Lab 01 - WordCount - EE 491F - Spr 2021
/// @date   27/01/2021 
///////////////////////////////////////////////////////////////////////////////

#include "count.h"

/* This function will count characters, words, and lines of a given file */
void count_file(FILE *file){

   extern int line_count;  
   extern int word_count;     
   extern int character_count;

   // Declare variable to keep track of current character in the input stream of fgetc
   char current_char;

   // Declare variable to determine whether or not we are currently in a word
   bool in_word = false;

   while( (current_char = fgetc(file)) != EOF ){

   // Character count will always increment as fgetc always reads a single unsigned character from the input stream if EOF is not reached
      character_count++;

   // 0x30 == '1', 0x39 == '9', 0x41 == 'A', 0x5a == 'Z', 0x61 == 'a' , 0x7a == 'z'
   // We are checking to see if the current_char is a letter or number, this will tell us if we are in a word or not
      if( (current_char >= 0x30 && current_char <= 0x39 ) || (current_char >= 0x41 && current_char <= 0x5a) || (current_char >= 0x61 && current_char <= 0x7a) ){
         in_word = true;
      }

   // 0x20 == ' ' , 0x0a == '\n' , 0x0d == '\r', 0x09 == '\t'
   // If we are in a word, and we see one of the above we can then increment the word count
      if ( (current_char == 0x20 || current_char == 0x0a  || current_char == 0x0d || current_char == 0x09) && in_word == true ){
         word_count++;
         in_word = false;
      }

   // Lastly we increment line count as well if a newline character is identified
      if( current_char == '\n' ){
         line_count++;
      }
   }
}
